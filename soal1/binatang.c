#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execv("/usr/bin/wget", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada binatang.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada binatang.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
    system("rm binatang.zip");
}

void printShift()
{
    char namaHewan[30];
    FILE *filePointer;

    system("ls | grep \".jpg$\" | shuf -n 1 > temp.txt");
    filePointer = fopen("temp.txt", "r");

    if(filePointer == NULL){
        printf("Gagal membuka file temp\n");
        exit(EXIT_FAILURE);
    }
    fscanf(filePointer, "%s", namaHewan);
    fclose(filePointer);
    system("rm temp.txt");

    printf("Hewan yang akan dijaga kali ini adalah ");
    for(int i = 0; (namaHewan + i) != NULL && namaHewan[i] != '_'; ++i)
        printf("%c", namaHewan[i]);
    printf("\n");
}

void zip(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan zip pada %s\n", dir);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0)
    {
        char temp[20];
        strcpy(temp, dir);
        strcat(temp, ".zip");

        char *argv[] = {"zip", "-q", "-r", "-m", temp, dir, NULL};
        execv("/usr/bin/zip", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses zip pada %s tidak berhenti dengan normal\n", dir);
        exit(EXIT_FAILURE);
    }
}

int main()
{
    /* 1A */
    download();
    unzip();

    /* 1B */
    printShift();

    /* 1C */
    system("mkdir HewanDarat HewanAmphibi HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
    system("mv *air.jpg HewanAir");
    
    /* 1D */
    zip("HewanDarat");
    zip("HewanAir");
    zip("HewanAmphibi");
    return 0;
}
