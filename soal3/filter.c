#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <signal.h>
#include <dirent.h>
#include <pwd.h>

char kiper[20][50];
char bek[20][50];
char gelandang[20][50];
char penyerang[20][50];

void categorize(char* name, int category) {
    // printf("%s\n", name);
    switch(category) {
    case 0:
        if(strstr(name, "_Kiper_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Kiper", NULL);
        }
        break;
    case 1:
        if(strstr(name, "_Bek_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Bek", NULL);
        }
        break;
    case 2:
        if(strstr(name, "_Gelandang_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Gelandang", NULL);
        }
        break;
    case 3:
        if(strstr(name, "_Penyerang_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Penyerang", NULL);
        }
        break;
    }
    exit(0);
}

// Comparison function for qsort
int comp(const void* a, const void* b) {
    int lenA = strlen((char*)a);
    int lenB = strlen((char*)b);
    return strcmp((char*)a + lenA - 6, (char*)b + lenB - 6) < 0;
}

// Get players and sort them based on rating
void getPlayers(char players[20][50], char* dir) {
    DIR *dp = opendir(dir);
    struct dirent *ep;
    int i = 0;

    while(ep = readdir(dp)) {
        if(strstr(ep->d_name, "_") != NULL)
            strcpy(players[i++], ep->d_name);
    }

    qsort(players, i, sizeof(players[0]), comp);

    // for(int j = 0; j < i; j++) {
    //     printf("%s\n", players[j]);
    // }
}

void buatTim(int bekn, int gelandangn, int strikern) {
    if(bekn+gelandangn+strikern+1 != 11) return;
    char filename[50];
    sprintf(filename, "Formation_%d-%d-%d.txt", bekn, gelandangn, strikern);
    FILE *fp = fopen(filename, "w+");
    fprintf(fp, "Formasi Team\n");
    fprintf(fp, "Kiper :\n");
    fprintf(fp, "%s\n",kiper[0]);
    fprintf(fp, "Bek :\n");
    for(int i = 0; i < bekn; i++) {
        fprintf(fp, "%s\n", bek[i]);
    }
    fprintf(fp, "Gelandang :\n");
    for(int i = 0; i < gelandangn; i++) {
        fprintf(fp, "%s\n", gelandang[i]);
    }
    fprintf(fp, "Penyerang :\n");
    for(int i = 0; i < strikern; i++) {
        fprintf(fp, "%s\n", penyerang[i]);
    }
    fprintf(fp, "%d-%d-%d", bekn, gelandangn, strikern);
    fclose(fp);
    __pid_t pid = fork();
    if(pid == 0) {
        struct passwd *pw = getpwuid(getuid());
        char *homedir = pw->pw_dir;
        execl("/usr/bin/mv", "mv", filename, homedir, NULL);
    }
}

int main() {
    char *path = "/home/arda294/sisop/praktikum2/soal3";
    char *link = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    DIR *dp;
    struct dirent *ep;
    umask(0);
    
    // Pid List
    __pid_t pid = fork();
    __pid_t child_pid;
    __pid_t child_child_pid;

    int status;

    chdir(path);
    if(pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", "players.zip", link, NULL);
    }
    waitpid(pid, &status, 0);
    pid = fork();
    if(pid == 0) {
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
    }
    waitpid(pid, &status, 0);
    // Delete non ManUtd
    pid = fork();
    if(pid == 0) {
        chdir("players");
        dp = opendir(".");
        while(ep = readdir(dp)) {
            if(strstr(ep->d_name, "ManUtd") == NULL && !(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0)) {
                if((child_pid = fork()) == 0) {
                    execl("/usr/bin/rm", "rm", ep->d_name, NULL);
                }
            }
        }
        exit(0);
    }
    waitpid(pid, &status, 0);
    // Categorize
    pid = fork();
    if(pid == 0) {
        chdir("players");
        // Create Folder
        if((child_pid = fork()) == 0) {
            execl("/usr/bin/mkdir", "mkdir", "Kiper", "Bek", "Gelandang", "Penyerang", NULL);
        }
        waitpid(child_pid, &status, 0);
        for(int i = 0; i < 4; i++) {
            // Move all to each category
            if((child_pid = fork()) == 0) {
                DIR *dpc = opendir(".");
                struct dirent *epc;
                while(epc = readdir(dpc)) {
                    if((child_pid = fork()) == 0) {
                        categorize(epc->d_name, i);
                    }
                }
            }
        }
        waitpid(child_pid, &status, 0);
        exit(0);
    }
    waitpid(pid, &status, 0);

    // Get players on each category
    getPlayers(kiper, "./players/Kiper");
    getPlayers(bek, "./players/Bek");
    getPlayers(gelandang, "./players/Gelandang");
    getPlayers(penyerang, "./players/Penyerang");

    // Makes the team and print it into a file
    buatTim(5,4,1);
    execl("/usr/bin/pwd", "pwd", NULL);
    return 0;
}