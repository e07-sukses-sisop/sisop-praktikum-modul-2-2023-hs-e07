# **Praktikum Modul 2**

## **Anggota Kelompok**

- 5025211235 - Ketut Arda Putra Mahotama

- 5025211003 - Clarissa Luna Maheswari

- 5025211127 - Nadif Mustafa

## **Soal 1**

### **Screenshot saat di-*run***

#### **1.A**

![image](/uploads/f1bfa5815d7d36b022fe0f7cc26d8f83/image.png)

#### **1.B**

![image](/uploads/b5b43671220eea3ccefd57af7bb9d5e9/image.png)

#### **1.C**

![image](/uploads/8984086f97e966e83b781baab5de4985/image.png)

#### **1.D**

![image](/uploads/f50c762cd2091bd532f66a4371cf8f47/image.png)

### **Cara pengerjaan**

Pada soal ini, kami membagi proses-proses yang menggunakan `exec` menjadi beberapa function, yaitu function `download()`, `unzip()`, dan `zip()`.

Selain itu, juga ada function `printShift()` yang nantinya akan digunakan pada soal `1B` untuk memilih hewan random dan menampilkannya pada terminal.

Pada setiap `spawning process` dilakukan pengecekan jika `(child id < 0)`, serta lakukan `wait` dan pengecekan jika proses child tidak berhenti dengan normal `(!WIFEXITED)`.

#### **1.A**

Pada subsoal ini diminta untuk mendownload sebuah file zip dari link gdrive yang diberikan pada soal, kemudian melakukan unzip file
tersebut.

Proses download dan unzip dilakukan di dalam `child process` pada function `download()` dan `unzip()` dengan menggunakan `execv`.

Berikut adalah code dari function `download()` :

```c
void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execv("/usr/bin/wget", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}
```

Berikut adalah argumen-argumen yang digunakan pada proses `execv` :

- `wget :` command pada linux untuk melakukan download.

- `-q :` salah satu option dari command `wget`, supaya jalannya proses download tidak ditampilkan pada terminal.

- `-O :` salah satu option dari command `wget`, supaya file yang sudah didownload bisa kita simpan dengan nama yang sesuai dengan keinginan kita.

- `download.zip :` nama file zip yang ingin kita simpan.

- `https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq :` link untuk mendownload file zip yang diminta.

Berikut adalah code dari function `unzip()` :

```c
void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada binatang.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada binatang.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
    system("rm binatang.zip");
}
```

Berikut adalah argumen-argumen yang digunakan pada proses `execv` :

- `unzip :` command pada linux untuk melakukan unzip.

- `-q :` salah satu option dari unzip, supaya jalannya proses unzip tidak ditampilkan pada terminal.

- `binatang.zip :` nama file yang ingin di-*unzip*.

Pada `parent process` dilakukan penghapusan file `"binatang.zip"` setelah `child process` selesai dikerjakan.

Proses penghapusan dilakukan dengan menggunakan function `system()`, yang di dalamnya diisi `"rm binatang.zip"`, dengan `rm` merupakan command pada linux untuk melakukan penghapusan.

Berikut ini adalah code untuk subsoal ini pada `main` :

```c
/* 1A */
download();
unzip();
```

#### **1.B**

Pada subsoal ini diminta untuk memilih secara acak file gambar yang sudah di-*unzip*, kemudian lakukan shift penjagaan pada hewan tersebut. Kami melakukan shift dengan cara print `"Hewan yang akan dijaga kali ini adalah <nama_hewan>"`.

Seluruh proses pada subsoal ini dilakukan pada function `printShift()`.

Pertama, pilih sebuah file `.jpg` random, kemudian hasil dari pemilihan tersebut ditulis di dalam sebuah file `.txt`, setelah itu print hasil tersebut pada terminal menggunakan format sesuai dengan yang sudah disebutkan di atas.

Berikut adalah code dari function `printShift()` :

```c
void printShift()
{
    char namaHewan[30];
    FILE *filePointer;

    system("ls | grep \".jpg$\" | shuf -n 1 > temp.txt");
    filePointer = fopen("temp.txt", "r");

    if(filePointer == NULL){
        printf("Gagal membuka file temp\n");
        exit(EXIT_FAILURE);
    }
    fscanf(filePointer, "%s", namaHewan);
    fclose(filePointer);
    system("rm temp.txt");

    printf("Hewan yang akan dijaga kali ini adalah ");
    for(int i = 0; (namaHewan + i) != NULL && namaHewan[i] != '_'; ++i)
        printf("%c", namaHewan[i]);
    printf("\n");
}
```

Berikut adalah urutan berjalannya function di atas :

- Deklarasi sebuah string bernama `namaHewan` serta sebuah `file pointer`.

- Menggunakan function `system()` dengan isi sebagai berikut :

    - `ls :` meng-*list* seluruh file dan folder pada `working directory`.
    
    - `grep \".jpg\" :` memilih seluruh file yang memiliki ekstensi `.jpg`.
    
    - `shuf -n 1 :` memilih 1 file secara random berdasarkan hasil `grep` tadi.

    - `> temp.txt :` me-*redirect* hasil dari `shuf` ke dalam file `temp.txt`.

- Membaca isi file `temp.txt` dan menyimpan hasilnya ke dalam string `namaHewan`.

- Print string `"Hewan yang akan dijaga kali ini adalah <nama_hewan>"`.

- `<nama_hewan>` diisi dengan string `namaHewan`, dengan cara iterasi karakter satu per satu dan berhenti ketika menemukan `"_"`, sehingga jenis hewan dan ekstensi file tidak ikut diprint.

Berikut ini adalah code dari subsoal ini pada `main` :

```c
/* 1B */
printShift();
```

#### **1.C**

Pada subsoal ini diminta untuk membuat folder `HewanDarat`, `HewanAmphibi`, dan `HewanAir`. Setelah itu, lakukan filter pada file-file gambar dan pindahkan sesuai dengan jenis hewannya.

Seluruh proses pada subsoal ini dijalankan di `main` dengan menggunakan function `system()`.

Berikut adalah code untuk subsoal ini :

```c
/* 1C */
system("mkdir HewanDarat HewanAmphibi HewanAir");
system("mv *darat.jpg HewanDarat");
system("mv *amphibi.jpg HewanAmphibi");
system("mv *air.jpg HewanAir");
```

Berikut adalah urutan berjalannya function di atas :

- Membuat folder `HewanDarat`, `HewanAmphibi`, dan `HewanAir` menggunakan command `mkdir`

- Memindahkan semua hewan darat ke folder `HewanDarat` menggunakan command `"mv *darat.jpg HewanDarat"`

- Memindahkan semua hewan amphibi ke folder `HewanAmphibi` menggunakan command `"mv *darat.jpg HewanAmphibi"`

- Memindahkan semua hewan air ke folder `HewanAir` menggunakan command `"mv *darat.jpg HewanAir"`

#### **1.D**

Pada subsoal ini diminta untuk melakukan zip pada masing-masing folder yang sudah dibuat pada soal `1C`, kemudian hapus seluruh folder tersebut.

Proses zip dilakukan di dalam `child process` pada function `zip()` dengan menggunakan `execv`.

Function `zip()` memiliki parameter `(char *dir)` yang akan menerima argument berupa nama folder yang akan di-*zip*.

Berikut adalah code dari function `zip()` :

```c
void zip(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan zip pada %s\n", dir);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0)
    {
        char temp[20];
        strcpy(temp, dir);
        strcat(temp, ".zip");

        char *argv[] = {"zip", "-q", "-r", "-m", temp, dir, NULL};
        execv("/usr/bin/zip", argv);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses zip pada %s tidak berhenti dengan normal\n", dir);
        exit(EXIT_FAILURE);
    }
}
```

Berikut adalah urutan berjalannya `child process` pada function di atas :

- Deklarasi sebuah string bernama `temp`, yang nantinya akan digunakan untuk menyimpan nama file `.zip` dari folder yang diminta.

- Menggunakan function `execv()` dengan argument sebagai berikut :

    - `zip :` command pada linux untuk melakukan zip.
    
    - `-q :` salah satu option dari command `zip`, supaya jalannya proses zip tidak ditampilkan pada terminal.
    
    - `-r :` salah satu option dari command `zip`, supaya zip dilakukan secara rekursif pada seluruh isi dari folder yang akan di-*zip*.

    - `-m :` salah satu option dari command `zip`, supaya folder / file asli dihapus setelah proses zip berhasil dijalankan.

    - `temp :` nama file zip yang akan disimpan.

    - `dir :` nama folder yang akan di-*zip*

Berikut adalah code untuk subsoal ini pada `main` :

```c
/* 1D */
zip("HewanDarat");
zip("HewanAir");
zip("HewanAmphibi");
```

### **Kendala yang dialami**

Tidak ada kendala yang kami alami pada soal ini.

### **Revisi**

Tidak ada revisi dari soal ini.

## **Soal 2**

### **Screenshot saat di-*run***

- Ketika program lukisan dijalankan (mode b) :
![Screenshot_2023-04-08_203929](/uploads/f1308866d5ce5abfffc3226a1712d7b8/Screenshot_2023-04-08_203929.png)

- Ketika program killer telah dijalankan dan telah selesai download semua gambar :

![Screenshot_2023-04-08_203929_2](/uploads/b30e1ccf00e9d605cbda97963233d862/Screenshot_2023-04-08_203929_2.png)

### **Cara pengerjaan**
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

#### **2.A**
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

Untuk mendapatkan tanggal dan waktu diperlukan library ``time.h`` dan penggunaanya seperti berikut :

```c
time(&epoch);
cur_time = localtime(&epoch);
strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time);
```

- ``time(&epoch)`` memasukkan UNIX Epoch kedalam variabel ``epoch``.
- ``cur_time = localtime(&epoch)`` digunakan untuk mencari current time.
- ``strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time)`` akan menyesuaikan current time dengan format ``YYYY-MM-dd_HH:mm:ss`` dan disimpan ke dalam variabel ``foldertime``.

#### **2.B**
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

```c
time(&epoch);
cur_time = localtime(&epoch);
chdir(path);
sprintf(link, "https://picsum.photos/%ld", epoch%1000 + 50);
strftime(datetime, 100, "%Y-%m-%d_%X", cur_time);
sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime);
execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL);
exit(0);
```

- karena pengambilan tanggal dan waktu untuk image dan folder berbeda, maka akan dilakukan pengambilan waktu untuk masing-masing image.
- ``sprintf(link, "https://picsum.photos/%ld", epoch%1000 + 50)`` dilakukan untuk mendapatkan link download image dengan ketentuan ``gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix``
- ``sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime)`` dilakukan untuk menyimpan informasi lokasi output image dan juga nama image tersebut.
- ``execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL)`` mendownload image dan meletakkan output ke folder masing-masing.
- semua hal diatas dilakukan 15 kali dengan jeda 5 detik sesuai dengan soal.

#### **2.C**
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

```c
zip_pid = fork();
if(zip_pid == 0) {
    execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL);
}
waitpid(zip_pid, &zipStatus, 0);
execl("/usr/bin/rm", "rm", "-r", foldertime, NULL);
exit(0);
```

- ``execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL)`` dilakukan untuk melakukan zip folder ketika telah selesai download semua gambar untuk folder tersebut
- ``execl("/usr/bin/rm", "rm", "-r", foldertime, NULL)`` akan menghapus folder yang telah di zip untuk menyimpan storage space

#### **2.D dan 2.E**
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

```c
void createKiller(int mode) {
    int status = 0;

    // Generate c code for killer program
    FILE *fp = fopen("killer.c", "w+");
    fprintf(fp, "#include<stdio.h>\n");
    fprintf(fp, "#include<stdlib.h>\n");
    fprintf(fp, "#include<unistd.h>\n");
    fprintf(fp, "int main() {\n");
    if(mode == 1) {
        fprintf(fp, "__pid_t pid = fork();\n");
        fprintf(fp, "if(pid == 0) execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"-e\", \"-c\", \"download_process\", NULL);\n");
    }
    fprintf(fp, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"-e\", \"-c\", \"folder_process\", NULL);\n");
    fprintf(fp, "return 0;}\n");
    fclose(fp);

    // Compile and remove c file
    pid_t killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
    waitpid(killer_pid, &status, 0);
    killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
}
```
- fungsi ``createKiller()`` akan mengcompile program killer untuk menghentikan process sesuai dengan input mode yaitu MODE_A (hentikan semua proses) dan MODE_B (hentikan hanya pembuatan folder baru) sebagai parameter fungsi tersebut.
- ``FILE *fp = fopen("killer.c", "w+");`` membuat file ``killer.c`` yang akan diisi dengan kode untuk killer program.
- ``execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL)`` akan mengcompile program killer.
- ``execl("/usr/bin/rm", "rm", "killer.c", NULL)`` akan menghapus file ``killer.c`` setelah dicompile.

Untuk menentukan mode yang diinginkan, maka program harus dapat menerima argumen

```c
if(argc == 1) {printf("Please use -a or -b flag as arguments\n"); exit(0);}
if(strcmp("-a",argv[1]) == 0) {
    printf("Launched with Mode A\n");
    createKiller(1);
} else if (strcmp("-b",argv[1]) == 0) {
    printf("Launched with Mode B\n");
    createKiller(2);
} else {
    printf("Invalid mode flag!\n");
    exit(1);
}
```

- ``createKiller(1)`` compile program killer untuk MODE_A.
- ``createKiller(2)`` compile program killer untuk MODE_B.

### **Kendala yang dialami**
Tidak ada kendala yang dialami dalam pengerjaan soal ini.

### Revisi
Tidak ada revisi untuk soal ini.

## **Soal 3**

### **Screenshot saat di-*run***

- Ketika program filter dijalankan : 
![Screenshot_2023-03-24_030315](/uploads/b1bbc17ecb0e53eb6bb2ca5a8cda0303/Screenshot_2023-03-24_030315.png)
error-error yang terdapat di console tidak memengaruhi kinerja program.

- Hasil output file .txt di home directory : 
![Screenshot_2023-03-24_030422](/uploads/bac415661aef33f3f59df2a9f2159372/Screenshot_2023-03-24_030422.png)

### **Cara pengerjaan**
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

Catatan:
- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

#### **3.A**
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

```c
pid = fork();
if(pid == 0) {
    execl("/usr/bin/wget", "wget", "-q", "-O", "players.zip", link, NULL);
}
waitpid(pid, &status, 0);
pid = fork();
if(pid == 0) {
    execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
}
pid = fork();
if(pid == 0) {
    execl("/usr/bin/rm", "rm", "players.zip", NULL);
}
```
- ``execl("/usr/bin/wget", "wget", "-q", "-O", "players.zip", link, NULL)`` akan mengunduh file ``players.zip`` yang berisi database para pemain bola.
- ``execl("/usr/bin/unzip", "unzip", "players.zip", NULL)`` akan unzip ``playres.zip``.
- ``execl("/usr/bin/rm", "rm", "players.zip", NULL)`` akan menghapus file ``players.zip`` setelah di unzip.

#### **3.B**
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory. 

```c
pid = fork();
if(pid == 0) {
    chdir("players");
    dp = opendir(".");
    while(ep = readdir(dp)) {
        if(strstr(ep->d_name, "ManUtd") == NULL && !(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0)) {
            if((child_pid = fork()) == 0) {
                execl("/usr/bin/rm", "rm", ep->d_name, NULL);
            }
        }
    }
    exit(0);
}
```

- ``chdir("players")`` akan mengubah directory menjadi dalam folder players.
- ``if(strstr(ep->d_name, "ManUtd") == NULL && !(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0))`` akan memastikan player yang dihapus bukan dari Manchester United. Selain itu agar directory ``.`` dan ``..`` tidak dihapus.
- ``execl("/usr/bin/rm", "rm", ep->d_name, NULL)`` akan menghapus sebuah player.

#### **3.C** 
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

Berikut adalah fungsi pembantu untuk memasukan player dalam kategori masing masing :
```c
void categorize(char* name, int category) {
    // printf("%s\n", name);
    switch(category) {
    case 0:
        if(strstr(name, "_Kiper_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Kiper", NULL);
        }
        break;
    case 1:
        if(strstr(name, "_Bek_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Bek", NULL);
        }
        break;
    case 2:
        if(strstr(name, "_Gelandang_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Gelandang", NULL);
        }
        break;
    case 3:
        if(strstr(name, "_Penyerang_") != NULL) {
            execl("/usr/bin/mv", "mv", name, "Penyerang", NULL);
        }
        break;
    }
    exit(0);
}
```
- ``strstr(name, "_Posisi_")`` mengecek posisi sebuah player.
- ``execl("/usr/bin/mv", "mv", name, "Posisi", NULL)`` memindahkan player ke folder posisi masing-masing.

```c
pid = fork();
    if(pid == 0) {
        chdir("players");
        // Create Folder
        if((child_pid = fork()) == 0) {
            execl("/usr/bin/mkdir", "mkdir", "Kiper", "Bek", "Gelandang", "Penyerang", NULL);
        }
        waitpid(child_pid, &status, 0);
        for(int i = 0; i < 4; i++) {
            // Move all to each category
            if((child_pid = fork()) == 0) {
                DIR *dpc = opendir(".");
                struct dirent *epc;
                while(epc = readdir(dpc)) {
                    if((child_pid = fork()) == 0) {
                        categorize(epc->d_name, i);
                    }
                }
            }
        }
        waitpid(child_pid, &status, 0);
        exit(0);
    }
```
- ``execl("/usr/bin/mkdir", "mkdir", "Kiper", "Bek", "Gelandang", "Penyerang", NULL)`` membuat folder untuk masing-masing posisi.
- ``categorize(epc->d_name, i)`` akan memindahkan player ke folder posisi masing-masing.

#### **3.D**
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi ``Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt`` dan akan ditaruh di ``/home/[users]/``

Deklarasi array posisi player :
```c
char kiper[20][50];
char bek[20][50];
char gelandang[20][50];
char penyerang[20][50];
```

Berikut adalah fungsi pembantu untuk memasukan player kedalam array dan juga sortir berdasarkan rating :
```c
int comp(const void* a, const void* b) {
    int lenA = strlen((char*)a);
    int lenB = strlen((char*)b);
    return strcmp((char*)a + lenA - 6, (char*)b + lenB - 6) < 0;
}

// Get players and sort them based on rating
void getPlayers(char players[20][50], char* dir) {
    DIR *dp = opendir(dir);
    struct dirent *ep;
    int i = 0;

    while(ep = readdir(dp)) {
        if(strstr(ep->d_name, "_") != NULL)
            strcpy(players[i++], ep->d_name);
    }

    qsort(players, i, sizeof(players[0]), comp);
}
```

- fungsi ``comp()`` akan digunakan oleh ``qsort()`` sebagai fungsi pembading rating player.
- ``strcpy(players[i++], ep->d_name)`` akan memasukan player kedalam masing-masing array posisi.
- ``qsort(players, i, sizeof(players[0]), comp)`` melakukan sortir berdasarkan rating

Kemudian ada fungsi berikut untuk output hasil kedalam file ``Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt`` :
```c
void buatTim(int bekn, int gelandangn, int strikern) {
    if(bekn+gelandangn+strikern+1 != 11) return;
    char filename[50];
    sprintf(filename, "Formation_%d-%d-%d.txt", bekn, gelandangn, strikern);
    FILE *fp = fopen(filename, "w+");
    fprintf(fp, "Formasi Team\n");
    fprintf(fp, "Kiper :\n");
    fprintf(fp, "%s\n",kiper[0]);
    fprintf(fp, "Bek :\n");
    for(int i = 0; i < bekn; i++) {
        fprintf(fp, "%s\n", bek[i]);
    }
    fprintf(fp, "Gelandang :\n");
    for(int i = 0; i < gelandangn; i++) {
        fprintf(fp, "%s\n", gelandang[i]);
    }
    fprintf(fp, "Penyerang :\n");
    for(int i = 0; i < strikern; i++) {
        fprintf(fp, "%s\n", penyerang[i]);
    }
    fprintf(fp, "%d-%d-%d", bekn, gelandangn, strikern);
    fclose(fp);
    __pid_t pid = fork();
    if(pid == 0) {
        struct passwd *pw = getpwuid(getuid());
        char *homedir = pw->pw_dir;
        execl("/usr/bin/mv", "mv", filename, homedir, NULL);
    }
}
```
- menerima argumen ``bekn``, ``gelandangn``, dan ``strikern`` untuk menentukan jumlah pemain di masing masing posisi (kiper sudah pasti 1).
- ``execl("/usr/bin/mv", "mv", filename, homedir, NULL)`` akan memindahkan hasil ke home directory user.

### **Kendala yang dialami**

Tidak ada kendala yang dialami dalam pengerjaan soal ini.

### **Revisi**

Tidak ada revisi untuk soal ini.

## **Soal 4**

### **Cara pengerjaan**

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk [ * ]** (value bebas), serta **path file** **.sh**.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error**.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini **berjalan dalam background** dan **hanya menerima satu config cron**.
- Bonus poin apabila CPU state minimum.

**Contoh untuk run**: 
```shell
/program \* 44 5 /home/Banabil/programcron.sh;
```
**Langkah Pengerjaan**:

1. Program dimulai dengan mengimpor beberapa header file seperti stdio.h, stdlib.h, string.h, sys/types.h, sys/wait.h, sys/stat.h, unistd.h, fcntl.h, time.h, signal.h, dan stdbool.h.
2. Kemudian, terdapat beberapa fungsi yang akan digunakan pada program ini, yaitu:
a. checkWildcard: fungsi untuk memeriksa apakah nilai asterisk ("-1") pada waktu saat ini sama dengan nilai yang diinginkan. Fungsi ini mengembalikan nilai boolean.
b. convertString: fungsi untuk mengonversi argumen string menjadi integer, dan memeriksa apakah nilai argumen valid atau tidak.
c. runScript: fungsi untuk menjalankan script yang diberikan pada argumen.
d. processSignal: fungsi untuk menangani sinyal SIGTERM dan SIGINT.
e. runDaemon: fungsi untuk menjalankan program dalam mode daemon.
3. Pada fungsi main, program akan memeriksa jumlah argumen yang diberikan dari command line. Jika argumen tidak sama dengan 5, program akan memberikan pesan error dan keluar.
4. Program akan mengonversi jam, menit, dan detik yang diberikan pada argumen 1, 2, dan 3 menjadi integer menggunakan fungsi convertString.
5. Program akan menjalankan runDaemon untuk menjalankan program dalam mode daemon. Hal ini akan membuat program berjalan di background tanpa perlu menampilkan outputnya pada terminal.
6. Program akan menangani sinyal SIGTERM dan SIGINT menggunakan fungsi processSignal.
7. Kemudian, program akan masuk ke dalam loop while(true) untuk mengecek waktu saat ini. Loop ini akan berjalan terus menerus hingga program dihentikan secara manual dengan menekan Ctrl-C pada terminal.
8. Pada setiap iterasi loop, program akan mendapatkan waktu saat ini menggunakan clock_gettime dan mengonversinya menjadi waktu lokal menggunakan localtime. Kemudian, program akan memeriksa apakah waktu saat ini sama dengan waktu yang diinginkan menggunakan fungsi checkWildcard. Jika sama, program akan menjalankan script yang diberikan pada argumen keempat menggunakan fungsi runScript. Program akan menjalankan script dalam sebuah child process dan menunggu hingga child process selesai menggunakan wait.
9. Program akan melakukan sleep(1) selama 1 detik pada setiap iterasi loop untuk memastikan program tidak terlalu banyak menggunakan sumber daya sistem.

**Cara Run**

1. Kompilasi program dengan menggunakan command gcc -o mainan mainan.c
2. Jalankan program dengan menggunakan command ./mainan <jam> <menit> <detik> <file_script>
3. <jam> : Jam yang diinginkan untuk menjalankan script, dalam range 0-23 atau "*" untuk opsi wildcard
<menit> : Menit yang diinginkan untuk menjalankan script, dalam range 0-59 atau "*" untuk opsi wildcard
<detik> : Detik yang diinginkan untuk menjalankan script, dalam range 0-59 atau "*" untuk opsi wildcard
<file_script> : Path menuju script yang ingin dijalankan

Program akan berjalan sebagai daemon. Untuk menghentikan program, gunakan command kill <PID>. 

contoh code .sh untuk dirun:
```shell
#!/bin/bash
# log file
log_file="/home/luna/sisop/mainan.log"
# current date and time
now=$(date +"%Y-%m-%d %H:%M:%S")
# log start of program
echo "[$now] proses dimulai" >> "$log_file"
# looping 5 kali dan log setiap iterasinya
for i in {1..5}
do
    echo "iterasi ke-$i" >> "$log_file"
    sleep 1
done

# log end of program
now=$(date +"%Y-%m-%d %H:%M:%S")
echo "[$now] proses selesai" >> "$log_file"

```
apabila file dijalankan, maka pada file mainan.log akan terlihat proses eksekusi dari program c Banabil:
![image](/uploads/11d1da7d4fed2b2b43d740202426b05a/image.png)

error yang ditampilkan apabila inputan tidak valid:
![image](/uploads/e3fb9716fbed14374684eed4041e2776/image.png)

### **Kendala yang dialami**
Pengerjaan soal bertepatan dengan deadline dan ujian lainnya sehingga waktu cukup terbatas. Pada awalnya, terdapat kesulitan pula dalam mengelola input dan memvalidasi apakah input tersebut sesuai dalam terminal. 

### **Revisi**
Tidak ada revisi dalam pengerjaan soal ini :)
