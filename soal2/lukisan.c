#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <signal.h>

void createKiller(int mode) {
    int status = 0;

    // Generate c code for killer program
    FILE *fp = fopen("killer.c", "w+");
    fprintf(fp, "#include<stdio.h>\n");
    fprintf(fp, "#include<stdlib.h>\n");
    fprintf(fp, "#include<unistd.h>\n");
    fprintf(fp, "int main() {\n");
    if(mode == 1) {
        fprintf(fp, "__pid_t pid = fork();\n");
        fprintf(fp, "if(pid == 0) execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"-e\", \"-c\", \"download_process\", NULL);\n");
    }
    fprintf(fp, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"-e\", \"-c\", \"folder_process\", NULL);\n");
    fprintf(fp, "return 0;}\n");
    fclose(fp);

    // Compile and remove c file
    pid_t killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
    waitpid(killer_pid, &status, 0);
    killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
}


int main(int argc, char* argv[]) {
    // Check program mode
    if(argc == 1) {printf("Please use -a or -b flag as arguments\n"); exit(0);}
    if(strcmp("-a",argv[1]) == 0) {
        printf("Launched with Mode A\n");
        createKiller(1);
    } else if (strcmp("-b",argv[1]) == 0) {
        printf("Launched with Mode B\n");
        createKiller(2);
    } else {
        printf("Invalid mode flag!\n");
        exit(1);
    }

    // Pid list
    __pid_t pid;
    __pid_t folder_pid;
    __pid_t child_pid;
    __pid_t zip_pid;

    int zipStatus = 0;

    // Download link
    char link[100];
    time_t epoch;
    struct tm *cur_time;

    // Folder and file names
    char datetime[20];
    char foldertime[20];
    char outputFile[100];
    char *path = "/home/arda294/sisop/praktikum2/soal2";

    // Make folder creator into a daemon
    pid = fork();
    if (pid < 0) {
        printf("Error in creating a daemon!\n");
        exit(1);
    }
    if (pid > 0) {
        printf("Daemon launcher exited\n");
        exit(0);
    }
    
    umask(0);
    pid = setsid();
    // Folder downloaders
    while(1) {
        folder_pid = fork();
        sprintf(argv[0], "folder_process");
        if(folder_pid == 0) {
            time(&epoch);
            cur_time = localtime(&epoch);
            strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time);
            mkdir(foldertime, 0777);
            // Image downloaders
            for(int j = 0; j < 15; j++) {
                child_pid = fork();
                sprintf(argv[0], "download_process");
                if(child_pid == 0) {
                    time(&epoch);
                    cur_time = localtime(&epoch);
                    chdir(path);
                    sprintf(link, "https://picsum.photos/%ld", epoch%1000 + 50);
                    strftime(datetime, 100, "%Y-%m-%d_%X", cur_time);
                    sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime);
                    execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL);
                    exit(0);
                }
                sleep(5);
            }
            // Zipper
            zip_pid = fork();
            if(zip_pid == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL);
            }
            waitpid(zip_pid, &zipStatus, 0);
            execl("/usr/bin/rm", "rm", "-r", foldertime, NULL);
            exit(0);
        }
        sleep(30);
    }
    return 0;
}
