#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdbool.h> 

// memeriksa apakah nilai asterisk sesuai dengan value goal/present
int checkWildcard(int presentValue, int goalValue) {
    return (presentValue == -1 || goalValue == -1 || presentValue == goalValue);
}
// konversi argumen string ke integer
int convertString(char *input_string, int maxValue) {
    int value;
    // mengecek argumen -> ada wildcard ga ngab
    if (strcmp(input_string, "*") == 0) {
        value = -1; // opsi ya
    } else {
        value = atoi(input_string); // opsi tidak
        if (value < 0 || value > maxValue) {
            fprintf(stderr, "Oh no! Nilai argumen tidak valid :(\n"); // pesan error
            exit(EXIT_FAILURE);
        }
    }
    return value;
}

// menjalankan script yang diberikan
void runScript(char *path_to_script) {
    char *script_args[] = {path_to_script, NULL};
    if (execv(path_to_script, script_args) == -1) {
        fprintf(stderr, "Oh no!Gagal menjalankan script: %s :(\n", path_to_script);
        exit(EXIT_FAILURE);
    }
}

// menangani sinyal SIGTERM dan SIGINT
void processSignal(int signal) {
    if (signal == SIGTERM || signal == SIGINT) {
        exit(EXIT_SUCCESS);
    }
}

// menjalankan program dalam mode daemon
void runDaemon() {
    pid_t pid, sid;
    pid = fork(); // simpan PID dr child

    if (pid < 0) { //fork gagal
        printf("Oh no!Gagal melakukan fork :'(\n"); // pesan error
        exit(EXIT_FAILURE);
    }
    if (pid > 0) { // fork berhasil, child process jalan
        exit(EXIT_SUCCESS);
    }

    umask(0); // ubah umasknya
    sid = setsid(); 

    if (sid < 0) { 
        printf("Oh no!Gagal membuat SID baru :'(\n"); // pesan error
        exit(EXIT_FAILURE);

    }

    if ((chdir("/")) < 0) { 
        printf("Oh no!Gagal ke root directory :'(\n"); // pesan error
        exit(EXIT_FAILURE);
    }
  
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main(int argc, char *argv[]) {
    int goalHour = 0, goalMinute = 0, goalSecond = 0;
    // periksa jumlah argumen yang diberikan
    if (argc != 5) {
        fprintf(stderr, "Oh no!Jumlah argumen tidak sesuai :'(\n"); //pesan error
        fprintf(stderr, "4 argumen: %s jam menit detik file_script\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    // mengonversi goal hour, minute, second ke integer
    goalHour = convertString(argv[1], 23);
    goalMinute = convertString(argv[2], 59);
    goalSecond = convertString(argv[3], 59);

    // menjalankan program di background
    runDaemon();
    // handle sinyal SIGTERM dan SIGINT
    signal(SIGTERM, processSignal);
    signal(SIGINT, processSignal);

    while (true) { // loop :)

        // mendapatkan waktu saat ini, sama dengan waktu sistem
        struct timespec presentTime;
        clock_gettime(CLOCK_REALTIME, &presentTime);
        // mengonversi waktu saat -> struktur waktu lokal
        struct tm *time_info = localtime(&presentTime.tv_sec);
        int presentHour = time_info->tm_hour;
        int presentMinute = time_info->tm_min;
        int presentSecond = time_info->tm_sec;

        // memeriksa waktu saat ini sesuai ga
        if (checkWildcard(presentHour, goalHour) && checkWildcard(presentMinute, goalMinute) && checkWildcard(presentSecond, goalSecond)) {
            pid_t pid = fork();
            if (pid == -1) {
                perror("Oh no!Gagal fork bro:/");
                exit(EXIT_FAILURE);
            } else if (pid == 0) { // child process
                runScript(argv[4]);
                exit(EXIT_SUCCESS);
            } else { // parent process
                wait(NULL); // tunggu child process selesai
            }
        }

        sleep(1);
    }
    return 0;
}






